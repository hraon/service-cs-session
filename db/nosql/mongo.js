const _ = require('lodash');
const mongoose = require('mongoose');
var configPath = __basedir + '/config/' + (!process.env.NODE_ENV ? 'local' : process.env.NODE_ENV);
let config = require(configPath);
var log = require(__basedir + '/core/logger');

mongoose.connect(`mongodb://${_.get(config, "mongoose.username")}:${_.get(config, "mongoose.password")}@${_.get(config, "mongoose.hostname")}:${_.get(config, "mongoose.port")}`,
    {dbName: `${_.get(config, "mongoose.database")}`, autoReconnect: true, poolSize: 10});
let mongoose_connection = mongoose.connection;
mongoose_connection.on('error', function (err) {
    log.error('MongoDB connection failed');
    log.error(err);
    process.exit(1);//stop the process if mongo connection fails
});
mongoose_connection.on('connected', function callback() {
    log.info("MongoDB connection successful!");
});
mongoose_connection.on('reconnected', function callback() {
    log.info("MongoDB re-connection successful!");
});
mongoose_connection.on('disconnected', function () {
    log.error('MongoDB connection terminated!');
    //process.exit(1);//stop the process if mongo connection fails
});

mongoose.set('debug', true);

module.exports = mongoose;