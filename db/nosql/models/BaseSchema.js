let baseSchema = new mongoose.Schema(
    {
        sys_created_at: {type: Date, default: Date.now()},
        sys_updated_at: {type: Date, default: Date.now()},
        user_created_at: {type: Number},
        user_updated_at: {type: Number},
    }
);

module.exports = baseSchema;