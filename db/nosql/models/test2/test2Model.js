let BaseSchema = require('../BaseSchema');
let DerivedModel = mongoose.model('Collection2Name',BaseSchema);
let test2Model = DerivedModel.discriminator('test2Model', new mongoose.Schema(
    {url: String}
));

module.exports = test2Model;