let BaseSchema = require('../BaseSchema');
let DerivedModel = mongoose.model('CollectionName', BaseSchema);
let test1Model = DerivedModel.discriminator('test1Model', new mongoose.Schema(
    {url: String}
));

module.exports = test1Model;