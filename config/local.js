module.exports = {
    "log_level":"",
    "db": {
        "database": "test_db",
        "port": 3306,
        "master": {
            "host": "127.0.0.1",
            "user": "root",
            "password": "root",
            "database": "test_db"
        },
        "slave": {
            "host": "127.0.0.1",
            "user": "root",
            "password": "root",
            "database": "test_db"
        }
    },
    "mongoose": {
        "hostname": "127.0.0.1",
        "username": "admin",
        "password": "admin123",
        "port": "27017",
        "database": "test_db",
        "authenticationDatabase": "admin"
    },
    "redis": {
        "port": 6379,
        "host": "127.0.0.1",
        "index": 1
    }
}
