global.__basedir = __dirname;//base directory

var express = require('express');
var app = express();
var morgan = require('morgan');
var path = require('path');
var configPath = './config/' + (!process.env.NODE_ENV ? 'local' : process.env.NODE_ENV);
console.log('Config file loaded from ' + configPath);
var config = require(configPath);
let configKeys = Object.keys(config);
if (configKeys.length > 0)
    app.set('config', config);
else {
    console.error('Invalid configuration!');
    process.exit(1);
}

var log = require('./core/logger');
var cache = require('./core/cache');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressPath = require('express-path');
var routes = require('./routes');
var managedRoutes = require('./utilities/routeManagerUtility').handle(routes);

const nf_conf = require('rc')('node-frame', {});

if (nf_conf._odm_) {
    global.mongoose = require('./db/nosql/mongo');
    mongoose.promise = Promise;
}
if (nf_conf._orm_) {
    global.sequelize = require('./db/sql/seq');
}
if (nf_conf._auto_loader_) {
    require('./lib/loader').loader();
    nf.log = log;
    nf.cache = cache;
}


var helmet = require('helmet');
app.use(helmet());

if (process.env.NODE_ENV !== 'production') {
    var swaggerUi = require('swagger-ui-express');
    var swaggerDocument = require('./swagger.json');
    app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
}

app.use(function (req, res, next) {
    log.info("Request Received: %s %s", req.method, req.url);
    req.log = log;
    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

app.use(morgan('combined', {stream: log.stream}));

expressPath(app, managedRoutes, {verbose: true});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    res.send({status_code: 404, message: "Not found", data: null});
});

// error handler
app.use(function (err, req, res, next) {
    log.error({
        method: req.method,
        url: req.url,
        payload: req.body,
        error_code: err.status,
        message: err.message,
        trace: err.stack
    });
    res.status(err.status || 500);
    res.send({status_code: err.status, message: err.message, data: null});
});

module.exports = app;
