// module.exports = [
//     ['/notifications/list', 'notificationController#getNotifications', 'authMiddleware#authenticated', 'queryParamsMiddleware#getPaginator',
//         'GET'],
// ];


module.exports = [
    {
        type: "get", route: "test1", middlewares: [], controller: "test1/test1Controller#create", version: "1"
    },
    {
        type: "get", route: "test1", middlewares: [], controller: "test1/test1Controller#get", version: "2"
    },
    {
        type: "get", route: "test2", middlewares: [], controller: "test1/test1Controller#delete", version: "2"
    }
];