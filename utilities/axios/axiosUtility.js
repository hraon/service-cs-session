const axios = require('axios');
const _ = require('lodash');

class axiosUtility {
    constructor() {
        this.config = {
            baseURL: 'https://jsonplaceholder.typicode.com',
            timeout: 1000,
            headers: {'Authorization': 'token'},
            responseType: 'json',// default
            responseEncoding: 'utf8', // default,
            maxRedirects: 5, // default,
            method: 'get'
        };
    }

    axiosRequest({url, method, data, headers}) {
        return new Promise((resolve, reject) => {
            this.config.url = url;
            if (headers && Object.keys(headers) > 0) {
                _.forEach(headers, (value, key) => {
                    this.config.headers[key] = value;
                });

            }

            method ? this.config.method = method : null;
            data ? this.config.data = data : null;

            axios.request(this.config)
                .then(function (response) {
                    nf.log.info(`destination url ${url} gave response ${JSON.stringify(response.data)}`);
                    resolve(response.data);
                })
                .catch(function (error) {
                    nf.log.error(`destination url ${url} gave response ${JSON.stringify(error)}`);
                    reject(error.message);
                });
        });

    }
}


module.exports = axiosUtility;