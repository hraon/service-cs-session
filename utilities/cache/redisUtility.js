module.exports = {

    get(key) {
        return new Promise((resolve, reject) => {
            try {
                nf.cache.get(key, function (err, result) {
                    if (err) {
                        nf.log.error(err);
                    }
                    else if (!result) {
                        nf.log.info(`Key ${key} not found`);
                        return resolve(`Key ${key} not found`);
                    }
                    else {
                        nf.log.info(`Key ${key} found`);
                        return resolve(result);
                    }
                });
            } catch (e) {
                nf.log.error(e);
                return reject(e);
            }

        });
    },
    //expire = 24 * 60 * 60 -> 24hours expiry
    set(key, value, expire = 24 * 60 * 60) {
        return new Promise((resolve, reject) => {
            try {
                nf.cache.set(key, value, 'EX', expire, function (err, result) {
                    if (err) {
                        nf.log.error(err);
                        return reject(err);
                    } else {
                        nf.log.info(`Key ${key} Value ${value} set`);
                        return resolve('Success');
                    }
                });
            } catch (e) {
                nf.log.error(e);
                return reject(e);
            }
        });
    },

    del(key) {
        return new Promise((resolve, reject) => {
            try {
                nf.cache.del(key, function (err, result) {
                    if (err) {
                        nf.log.error(err);
                    }
                    else if (result === 0) {
                        nf.log.info(`Key ${key} not found`);
                        return resolve(`Key ${key} not found`);
                    }
                    else {
                        nf.log.info(`Key ${key} deleted`);
                        return resolve('Success');
                    }
                });
            } catch (e) {
                nf.log.error(e);
                return reject(e);
            }
        });
    },
    //Documentation : https://redis.io/commands/set
    setnx(key, value, expire = 24 * 60 * 60) {
        return new Promise((resolve, reject) => {
            try {
                nf.cache.set(key, value, 'NX', 'EX', expire, function (err, result) {
                    if (err) {
                        nf.log.error(err);
                        return reject(err);
                    } else {
                        nf.log.info(`Key ${key} Value ${value} set`);
                        return resolve('Success');
                    }
                });
            } catch (e) {
                nf.log.error(e);
                return reject(e);
            }
        });
    },
}