module.exports = {
    create: function (req, res) {
        nf.utilities.cache.redisUtility.set('a', 2).then(result => {
            return res.send(result);
        }).catch(err => {
            return res.send(err);
        })
    },
    get: function (req, res) {
        let au = new nf.utilities.axios.axiosUtility();

        au.axiosRequest({url: '/posts/1'}).then(result => {
            return res.send(result);
        }).catch(err => {
            return res.send(err);
        })
    },
    delete: function (req, res) {
        nf.utilities.cache.redisUtility.del('a').then(result => {
            return res.send(result);
        }).catch(err => {
            return res.send(err);
        })
    }
}