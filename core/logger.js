let winston = require('winston');
require('winston-daily-rotate-file');
let appRoot = require('app-root-path');

let options = {
    file: {
        level: 'info',
        filename: `${appRoot}/logs/app.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
    },
    rotate_file: {
        filename: 'app-%DATE%.log',
        datePattern: 'YYYY-MM-DD-HH',
        zippedArchive: true,
        maxSize: '5m',//5MB
        maxFiles: '14d',
        json: true,
        dirname: `${appRoot}/logs`
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
};


var logger = new winston.Logger({
    transports: [
        new winston.transports.File(options.file),
        new winston.transports.DailyRotateFile(options.rotate_file),
        new winston.transports.Console(options.console),
    ],
    exitOnError: false
});

logger.stream = {
    write: function (message, encoding) {
        logger.info(message);
    }
};

module.exports = logger;
