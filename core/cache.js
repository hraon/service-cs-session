var Redis = require('ioredis');

var configPath = __basedir + '/config/' + (!process.env.NODE_ENV ? 'local' : process.env.NODE_ENV);
var config = require(configPath);
var log = require(__basedir + '/core/logger');

var redis = new Redis({
    port: config.redis.port,          // Redis port
    host: config.redis.host,   // Redis host
    db: config.redis.index,
    retryStrategy: function (times) {
        var delay = Math.min(times * 50, 2000);
        return delay;
    }
});

redis.on('connect', () => {
    log.info("Redis connection successful!");
});

redis.on('error', () => {
    log.error('Redis connection failed');
    //process.exit(1);//stop the process if redis connection fails
});
module.exports = redis;