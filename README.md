HELLO 76
# node-frame

Node js framework for microservice applications

Salient features :

* Rest-ful routes with versioning
* Error Handling using custom package(node-frame-errors)
* Middlewares
* Global variable (*nf*) which is loaded once the application is started 
* Service wrapper (Redis,Log etc..) through utility classes
* Logging & Rotation based on log file size and date using winston & morgan
* Uncaught exceptions (if any) are handled by winston


Sample run command 


```
NODE_ENV=test|production|dev PORT=3000 npm start
```


_*Default environment is local ( config/lib/local.js )*_
